import io.github.classgraph.ClassGraph
import java.io.File
import java.net.URL
import java.net.URLClassLoader
import java.nio.file.Paths

fun classpath(): List<File> =
    ((Thread.currentThread().contextClassLoader as URLClassLoader).urLs ?: emptyArray<URL>()).toList()
        .filterNot { it.path.contains("Java/JavaVirtualMachines") }
        .filterNot { it.path.contains("JetBrains/Toolbox/") }
        .map { Paths.get(it.toURI()).toFile() }

fun classgraphR(): List<File> =
    ClassGraph().classpathURIs.filterNotNull().map { Paths.get(it).toFile() }