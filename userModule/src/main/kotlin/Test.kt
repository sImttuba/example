import arrow.fx.IO
import arrow.fx.extensions.fx
import arrow.fx.extensions.io.unsafeRun.runBlocking
import arrow.unsafe

val myClasspath = classpath()

val myClasspathR = classgraphR()

fun ctx(): IO<Unit> = IO.fx {
    val elements = !effect { classgraphR() }
    !effect {println(elements)}
}

fun main() =
    unsafe {
        runBlocking {
            ctx()
        }
    }
